/**************************************/
/* resize bitmap */
/* Tarik Fdil <tfdil@sagma.ma> */
/* mars 2005 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#include <tiffio.h>

void resize_bitmap(int w, int h, uint32 *buf, int wr, int hr, uint32 *bufr)
{
int x,y,x0,y0;
double xr,yr;

xr = (double) w / (double)wr;
yr = (double) h/ (double)hr;

for(x=0; x < w; x++) {
	for(y=0; y < h; y++) {
		x0 = (int) ((double)x / xr);
		y0 = (int)((double)y / yr);
		bufr[y0*wr+x0] = buf[y*w+x];
	}
}

}
