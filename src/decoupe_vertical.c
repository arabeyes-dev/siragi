/**************************************/
/* OCR arabic
/* segmenting a line into charcters (see automata below)
/* Tarik Fdil <tfdil@sagma.ma> */
/* mars 2005 */
/* Licence GPL cf. http://www.gnu.org */
/* Automata :
Alphabet :
	- b : blank (hist=0) or end of line
	- s : separator (hist >0 and < S)
	- c : character  (hist > S)
	- e : end of line
States (5 states : init, car, sep, blanc and carp)
 	init :	- transitions  b to init, c or s to car, e to end
		- actions : none

	car :	- transitions :  c to car, s to sep, b or e to blanc
		- actions :  start of char

	sep :	- transitions : s to sep, c to car, b or e to blanc 
		- actions : end of char

	blanc :	- transitions : b to blanc, c or s to car, e to end
		- actions : end of char, start of blanc

	carb : 	- transitions : c to carb, b or e to blanc, s to sep
		- actions : end of blanc, start of char

	end : 	- transitions : none
		- actions : end of char or blanc
*/
/**************************************/

#define isblack(x,y) (buf[(y)*w+(x)] & 0x00FFFFFF)?0:1
#define S 7 
#include <stdlib.h>

static int compar(int *a,int *b) { return *b - *a;}

/* get input of automata */
#define GET_INPUT() do {c = (n>=w)? 'e' : (!hist[n])? 'b' : (hist[n]<=S)? 's' : 'c'; n++;} while(0);

void decoupe_vertical(	unsigned int w, unsigned int h,unsigned int *buf, /* width, height and pixels of image */
			int *char_start, 	/* array of characters start positions */ 
			int *char_end, 		/* array of characters end positions */ 
			int *nchar, 		/* number of characters found */
			int *blanc)             /* array of values 1 or 0, 1 if corresponding char is blank */
{
int x,y,z,hist[w],line[h],n,m,y0,y1;
char c;
float l;

/* find base line  */
m=z=0;
for(y=0; y<h; y++) {
	n = 0;
	for(x=0; x<w; x++) {
		if(isblack(x,y)) n++;
	}
	if(n>m) {m=n; z=y;}
}

/* printf("*********** line non tri� :\n");
for(y=0; y<h; y++)printf("line[%u]=%u\n",y,line[y]);
qsort(line,h,sizeof(int),compar);
printf("*********** line tri� :\n");
for(y=0; y<h; y++)printf("line[%u]=%u\n",y,line[y]);

y0 = ((z-S)>=0)?z-S:0;
y1 = ((z+S)<h)?z+S:h; */

/* calculate histogram  */
for(x=0; x<w; x++) {
	hist[x] = 0;
	for(y=0; y<h; y++) {
		if(isblack(x,y)) hist[x]++;
	}
}

*nchar=0;
n=0;
ini :
	GET_INPUT();
	switch(c) {
		case 'b' : goto ini;
		case 'c' : goto car;
		case 's' : goto car;
		case 'e' : goto end;
	}
car : 
	/* start of char */
	char_start[(*nchar)] = n;
	blanc[(*nchar)] = 0;
	(*nchar)++;
	do {
		GET_INPUT();
	} while(c == 'c');
	switch(c) {
		case 'b' : goto blanc;
		case 's' : goto sep;
		case 'e' : goto blanc;
	}
sep :
	/* end of char */
	char_end[(*nchar)-1] = n-1;
	do {
		GET_INPUT();
	} while(c == 's');
	switch(c) {
		case 'b' : goto blanc;
		case 'c' : goto car;
		case 'e' : goto blanc;
	}
blanc :
	/* end of char */
	char_end[(*nchar)-1] = n-1;
	/* start of blank */
	char_start[(*nchar)] = n;
	blanc[(*nchar)] = 1;
	(*nchar)++;
	do {
		GET_INPUT();
	} while(c == 'b');
	switch(c) {
		case 'c' : goto carb;
		case 's' : goto carb;
		case 'e' : goto end;
	}
carb :
	/* end of blank */
	char_end[(*nchar)-1] = n-1;
	/* start of char */
	char_start[(*nchar)] = n;
	blanc[(*nchar)] = 0;
	(*nchar)++;
	do {
		GET_INPUT();
	} while(c == 'c');
	switch(c) {
		case 'b' : goto blanc;
		case 's' : goto sep;
		case 'e' : goto blanc;
	}
end:
	/* end of blank/char */
	char_end[(*nchar)-1] = n-1;

/* calculate average width of white spaces */
n=0;
l=0;
for(x=0; x<(*nchar); x++) 
	if(blanc[x]) {
		l += (char_end[x]-char_start[x]);
		n++;
	}
l /= (float)n;
/* consider that every white space less than average is not a word separator */
for(x=0; x<(*nchar); x++) 
	if(blanc[x] && (char_end[x]-char_start[x]) < l) blanc[x]=2;

}
