/**************************************/
/* display_bitmap */
/* Tarik Fdil <tfdil@sagma.ma> */
/* Avril 2004 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#include <tiffio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>


void display_bitmap(int w, int h,uint32 *buf, int same,int ww,int hh)
{
static Display *dis;
static Window win;
static GC gc;
static Colormap colormap;
static XColor black_col,white_col,rgb_col;
static XEvent event;
static long eventmask = ExposureMask|KeyPressMask;
static char white[] = "#FFFFFF";
static char black[] = "#000000";
static char rgb[8];
static int i,x,y;
static int xmargin=10;
static int ymargin=10;
static int n,npixels;
static unsigned char r,g,b;
static float taux;
static int width,height;
uint32 *raster;

width=w;
height=h;
if(height > hh)  {
	taux = (float)hh/(float)height;
	height = hh;
	width = (int)(taux * width);
}
if(width > ww)  {
	taux = (float)ww/(float)width;
	width = ww;
	height = (int)(taux * height);
}

if(width != w || height != h) {
	npixels = width * height ;
	raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32)); 
	resize_bitmap(w,h,buf,width,height,raster);
} else raster = buf;

if(!same) {  /* use a new window not the previous one */
	dis = XOpenDisplay(NULL);
	win = XCreateSimpleWindow(dis, RootWindow(dis, 0), 1, 1, ww+2*xmargin, hh+2*ymargin, 0, 
		  WhitePixel (dis, 0), WhitePixel(dis, 0));
	XMapWindow(dis, win);
	gc = XCreateGC(dis, win, 0, 0);
	colormap = DefaultColormap(dis, 0);
	XParseColor(dis, colormap, white, &white_col);
	XParseColor(dis, colormap, black, &black_col);
	XAllocColor(dis, colormap, &white_col);
	XAllocColor(dis, colormap, &black_col);
}
do {
	/* erase window */
	XSetForeground(dis, gc, white_col.pixel);
	XFillRectangle(dis, win, gc, 0, 0, ww+2*xmargin,hh+2*ymargin);
	XSetForeground(dis, gc, black_col.pixel);
	for(i=0; i<width*height; i++) {
		r = (raster[i]) & 0xff;
		g = ((raster[i]) >> 8) & 0xff;;
		b = ((raster[i]) >> 16) & 0xff;;
		if(r==255 && g==255 && b==255) continue; 
		sprintf(rgb,"#%02X%02X%02X",r,g,b);
		XParseColor(dis, colormap, rgb, &rgb_col);
		XAllocColor(dis, colormap, &rgb_col);
		XSetForeground(dis, gc, rgb_col.pixel); 
		x = i%width + xmargin;
		y = height-(i/width + ymargin);
		XDrawPoint(dis,win,gc,x,y); 
	}
	XFlush(dis);
	XSelectInput(dis, win,eventmask); 
	XWindowEvent(dis,win,eventmask,&event);
} while (event.type != KeyPress);
_TIFFfree(raster); 
}
