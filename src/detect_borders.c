/**************************************/
/* detect border */
/* Tarik Fdil <tfdil@sagma.ma> */
/* december 2004 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#include<tiffio.h>
#define max(a,b) ((a)>(b))?(a):(b)
#define min(a,b) ((a)<(b))?(a):(b)
#define isblack(a,b) (buf[(b)*w+(a)] & 0x00FFFFFF)?0:1
#define iswhite(a,b) (buf[(b)*w+(a)] & 0x00FFFFFF ^ 0x00FFFFFF)?0:1

void detect_borders(uint32 w, uint32 h,uint32 *buf)
{
uint32 *raster,n;
uint32 red=0x000000ff,x,y,npixels,c;

raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32)); 

if (!raster) return;

for(n=0; n<w*h; n++) raster[n]=buf[n];

for (x=1; x<(w-1); x++)
for (y=1; y<(h-1); y++)
{	c = buf[y*w+x];
	if(     buf[(y+1)*w+x] != c ||
		buf[(y+1)*w+x] != c ||
		buf[(y+1)*w+x-1] != c ||
		buf[(y-1)*w+x] != c ||
		buf[(y-1)*w+x+1] != c ||
		buf[(y-1)*w+x-1] != c ||
		buf[y*w+x+1] != c ||
		buf[y*w+x-1] != c 
	 ) raster[y*w+x]=red;
}

for (x=1; x<(w-1); x++) for (y=1; y<(h-1); y++) buf[y*w+x] = raster[y*w+x];

}

