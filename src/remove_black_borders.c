/**************************************/
/* remove_black_borders
/* Tarik Fdil <tfdil@sagma.ma> */
/* decembre 2004 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#define isblack(x,y) (buf[(y)*w+(x)] & 0x00FFFFFF)?0:1

void remove_black_borders(unsigned int w, unsigned int h,unsigned int *buf)
{
int x,y;
for(y=0; y<h; y++) {
	for(x=0; x<w; x++) {
		if(!(isblack(x,y))) break;
		buf[y*w+x] = 0xffffffff;
	}
	for(x=w-1; x>=w-1; x--) {
		if(!(isblack(x,y))) break;
		buf[y*w+x] = 0xffffffff;
	}
}

for(x=0; x<w; x++) {
	for(y=0; y<h; y++) {
		if(!(isblack(x,y)) ) break;
		buf[y*w+x] = 0xffffffff;
	}
	for(y=h-1; y>=h-1; y--) {
		if(!(isblack(x,y))) break;
		buf[y*w+x] = 0xffffffff;
	}
}

}
