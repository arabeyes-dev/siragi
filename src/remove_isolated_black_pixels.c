/**************************************/
/* remove_isolated_black_pixels */
/* Tarik Fdil <tfdil@sagma.ma> */
/* decembre 2004 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#define max(a,b) ((a)>(b))?(a):(b)
#define min(a,b) ((a)<(b))?(a):(b)
#define isblack(x,y) (buf[(y)*w+(x)] & 0x00FFFFFF)?0:1
#define iswhite(x,y) (buf[(y)*w+(x)] & 0x00FFFFFF)?1:0

void remove_isolated_black_pixels(unsigned int w, unsigned int h,unsigned int *buf)
{
int i,x,y;

for(x=0; x<w; x++) {
	for(y=0; y<h; y++) {
		if(iswhite(x,y)) break;
		if(	iswhite(x,min(y+1,h-1)) &&
			iswhite(x,max(y-1,0)) &&
			iswhite(min(x+1,w-1),y) &&
			iswhite(max(x-1,0),y) &&
			iswhite(max(x-1,0),max(y-1,0)) &&
			iswhite(max(x-1,0),min(y+1,h-1)) &&
			iswhite(min(x+1,w-1),max(y-1,0)) &&
			iswhite(min(x+1,w-1),min(y+1,h-1))) buf[y*w+x] |= 0x00FFFFFF;
	}
}

}
