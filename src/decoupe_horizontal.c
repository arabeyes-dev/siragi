/**************************************/
/* d�coupage en lignes
/* Tarik Fdil <tfdil@sagma.ma> */
/* mars 2005 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#define isblack(x,y) (buf[(y)*w+(x)] & 0x00FFFFFF)?0:1
#define S 0

void decoupe_horizontal(unsigned int w, unsigned int h,unsigned int *buf,int *pos, int *nlig)
{
int x,y,hist[h],trait;

*nlig=0;
for(y=0; y<h; y++) {
	hist[y] = 0;
	for(x=0; x<w; x++) {
		if(isblack(x,y)) hist[y]++;
	}
	if(hist[y] > S) {
		trait=1;
	} else {
		if(trait) {
			trait=0;
			pos[*nlig] = y;
			(*nlig)++;
		}
	}
}

}
