/**************************************/
/* segmentation d'un fichier TIFF pour l'OCR */
/* TIFF file reader utilisant libtiff http://www.libtiff.org */
/* Tarik Fdil <tfdil@sagma.ma> */
/* mars 2005 */
/* Licence GPL cf. http://www.gnu.org */
/**************************************/

#include <tiffio.h>
#define MAXLIG 200
#define MAXCHAR 200

uint32 *clip_codbar(uint32 *w, uint32 *h,uint32 *buf);
char *read_codbar(uint32 w, uint32 h,uint32 *buf);
void detect_borders(uint32 w, uint32 h,uint32 *buf);

void print_usage()
{
printf("\nusage : segment [file]\n");
}

main(int argc, char **argv)
{ TIFF *tif;
  char file_name[30];
  char opt[3];
  int i,j,n,firstime;
  uint32 w, h,iw,ih,x,y,jw,jh; 
  size_t npixels; 
  uint32* raster,*rasterc,*rasterr;
  int lignes[MAXLIG],nlignes,char_start[MAXCHAR],char_end[MAXCHAR],nchar,blanc[MAXCHAR];
uint32 red=0x000000ff, c;
  uint32 bb[]={	0x00000000,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
		0x00000000,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
		0x00000000,0x00000000,0x00000000,0x00000000,
		0x00000000,0xFFFFFFFF,0xFFFFFFFF,0x00000000,
		0x00000000,0x00000000,0x00000000,0x00000000};

if(argc != 2)  {
	print_usage();
	exit(0);
}

strcpy(file_name,argv[1]);


if((tif = TIFFOpen(file_name,"r")) == NULL) {
	perror("impossible d'ouvrir le fichier");
	exit(1);
}

TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w); 
TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h); 
printf("\twidth=%u\n",w);
printf("\theigth=%u\n",h);
npixels = w * h; 
raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32)); 
if (raster != NULL) { 
	if (TIFFReadRGBAImage(tif, w, h, raster, 0)) {
		remove_isolated_black_pixels(w,h,raster);
		decoupe_horizontal(w,h,raster,lignes,&nlignes); 
		display_bitmap(w,h,raster,0,400,600);  
		firstime=1;
		for(i=nlignes-2; i>=0; i--) {
			ih = lignes[i+1] - lignes[i];
			iw = w;
			npixels = iw * ih ;
			rasterc = (uint32*) _TIFFmalloc(npixels * sizeof (uint32)); 
			for(x=0; x<iw; x++) for(y=0; y<ih; y++) rasterc[y*iw+x] = raster[(y+lignes[i])*w+x];
			decoupe_vertical(iw,ih,rasterc,char_start,char_end,&nchar,blanc); 
			for(j=nchar-1; j>=0; j--) {
				if(blanc[j] == 1) printf("blanc ...\n");
				if(blanc[j] == 2)  continue;
				jw = char_end[j] - char_start[j];
				jh = ih;
				npixels = jw * jh ;
				rasterr = (uint32*) _TIFFmalloc(npixels * sizeof (uint32)); 
				for(x=0; x<jw; x++) for(y=0; y<jh; y++) 
				rasterr[y*jw+x] = rasterc[y*iw+x+char_start[j]];
				display_bitmap(jw,jh,rasterr,firstime?0:1,100,100);
				firstime=0;
			}  
		}
	}
	_TIFFfree(raster); 
}
TIFFClose(tif);
getchar();
}
